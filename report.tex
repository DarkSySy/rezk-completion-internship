\documentclass{article}


% Packages
	\usepackage[utf8]{inputenc}
	\usepackage{color}
	\usepackage{verbatim}
	\usepackage{listings}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{amsthm}
	\usepackage{mathrsfs}
	\usepackage{graphicx}
	\usepackage{bbm}
	\usepackage{biblatex}
	\usepackage[unicode]{hyperref}
	\usepackage{indentfirst}
	% \usepackage[standard]{ntheorem}
	\usepackage{algorithm}
	\usepackage[noend]{algpseudocode}
	\usepackage{tikz}
	\usepackage{tikz-cd}
	\usepackage{enumitem}
	\usepackage[bb=boondox]{mathalfa}

% New commands and environments
	\newcommand{\hs}{\hspace{0.7cm}}
	\newcommand{\be}{\begin{equation}}
	\newcommand{\ee}{\end{equation}}
	\newcommand{\bee}{\begin{eqnarray}}
	\newcommand{\eee}{\end{eqnarray}}
	\newcommand{\fin}{\nonumber\\}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\LL}{\mathcal{L}}
	\newcommand{\U}{\mathcal{U}}
	\newcommand{\C}{\mathcal{C}}
	\newcommand{\wh}[1]{\widehat{#1}}
	\newcommand{\refl}[1]{\text{refl}_{#1}}
	\newcommand{\fo}[2]{\text{Fanout}_{#1}(#2)}
	\newcommand{\itoi}{\text{idtoind}}
	\newcommand{\itoc}{\text{idtocon}}
	\newcommand{\itoe}{\text{idtoeqv}}
	\newcommand{\id}[1]{\textbf{id}_{#1}}
	\newtheorem{theorem}{Theorem}[section]
	\newtheorem{lemma}[theorem]{Lemma}
	\newtheorem{definition}[theorem]{Definition}
	\newtheorem{example}[theorem]{Example}
	% \newtheorem{corollary}{Corollary}
	% \newtheorem{prop}{Proposition}

\title{Anonymous Objects: Another Proof of the Rezk Completion for Categories}
\author{Sylvain Gay}

\addbibresource{biblio.bib}

\begin{document}

\maketitle

\section*{Abstract}

	When studying categories in HoTT/UF, it turns out that the well-behaved definition is that of univalent categories, that is, categories such that isomorphisms between objects are equivalent to identities between them. It has been shown that any category is weakly equivalent to a univalent one; this operation is known as Rezk completion or stack completion.

	We provide another construction of the Rezk completion for categories, with the hope that it can be more easily generalized for other (higher) categorical structures. This new construction uses a concept of \emph{anonymous objects} for categories, which we defined for a general abstract notion of structures.


\newpage


\section*{Introduction}
	\label{sec:intro}

	Voevodsky \cite{hott} designed the Univalent Foundations (UF) as a foundational language for mathematics, in which constructions are invariant under equivalence of structures. It uses the language of Homotopy Type Theory (HoTT) and Martin-Löf type theory: types have identity types and are regarded as higher groupoids. The Univalence Axiom (UA) states that identities between types are equivalent to equivalences between them. When studying categories within HoTT/UF, a similar situation arises: we have identities and isomorphims between them, but they do not necessarily coincide. Univalent categories \cite{rezk_completion} are (pre)categories such that these two relations do coincide. They are better behaved, but some categories we study in practice are not univalent. The \emph{Rezk Completion} provides a univalent replacement of any category: every category is weakly equivalent to a univalent one.

	Built on Makkai's first order logic with dependent sorts \cite{folds}, the Univalence Principle \cite{up} develops a language of signatures for set-based, categorical and higher-categorical structures. It uses Two-Level Type Theory \cite{tltt} (2LTT) in order to treat higher categorical dimensions uniformly. Thanks to a notion of indiscernibilities between objects, they define univalent structures similarly to univalent categories. Although there has been proofs of results similar to a Rezk Completion for some other categorical structures, such as monoidal categories \cite{monoidal} and bicategories \cite{bicategories}, there is no proof of such a result for general categorical and higher-categorical structures.

	Building on the signatures, structures and indiscernibilities in \cite{up}, we define a notion of anonymous objects for structures, which is more general than the notion of indiscernibilities and has similarities with what we expect for a Rezk Completion. We use this new notion to give another proof of the Rezk Completion for categories.

	Section \ref{sec:intro-hott} recalls some elementary notions of HoTT/UF, in particular categories and univalent categories within HoTT/UF. Section \ref{sec:dstr-ex} gives some examples of signatures and structures defined within the framework of \cite{up}. Section \ref{sec:anon-ex} explains the notion of anonymous objects for these examples. Section \ref{sec:fun-sig} recalls the formalization of signatures and structures in \cite{up}, followed by to our new concept of anonymous objects. Section \ref{sec:rezk-cat} provides a new proof of the Rezk Completion for categories, using anonymous objects.

\newpage






\section{Introduction to HoTT/UF}
	\label{sec:intro-hott}
	
	This section is a very brief summary of some elements found in \cite{hott}. It can be skipped by any reader familiar with HoTT/UF and categories within it.
	
	\subsection{Homotopy Type Theory and the Univalence Axiom}

		\paragraph{Identity types.} In addition to the usual type formers, Homotopy Type Theory (HoTT) introduces (Martin-Löf) \emph{identity types}. They represents paths between objects of the space. For each type $A:\U$ of our universe $\U$ and $a,b:A$, there is a type $a=_Ab$ or simply $a=b$. For any $a:A$, we have a path $\refl{a}:a=a$. Importantly, there can be multiple elements of $a=b$: it behaves like homotopies rather than set-theoretic equality.

		We can define maps out of identity types by \emph{path induction}: in order to define maps out of the identity types of some $T: \U$, it suffices to specify the image of $\refl{x}$ for all $x:T$.
		Moreover, the identity type is \emph{transportable}: for $p:a=b$ and $P: A \rightarrow \U$ a type family, any $u: P(a)$ induces an element $transport_p^P(u): P(b)$. We write $p_*(u)$ when the context is clear.

		\paragraph{Homotopy level.} Since identity types are types, they have their own identity types, and so on: this builds a tower of identity types. In some cases, this tower is trivial after a certain point.

		\emph{Contractible} types such as $\mathbb 1$ have a unique element (up to identity): for a type $T$, we define $isContr(T) :\equiv \sum_{x:T} \prod_{y:T} x=y$. The existence of $x:T$ is the fact that $T$ is \emph{inhabited}.

		\emph{Propositions} such as $\mathbb 0$ or $\mathbb 1$ have contractible identities: any two objects of a proposition (or \emph{mere} proposition) are equal. We define $isProp(T) :\equiv \prod_{x,y:T} x=y$. There is an operation of \emph{propositional truncation}: for any type $T$, there is a type $||T||$ and a map $|\cdot| : T \rightarrow ||T||$ such that $||T||$ is a mere proposition. This operation is used for the logical $\exists$: because the $\sum$ constructor does not preserve mere propositions, we translate $\exists x:T, P(x)$ by $||\sum_{x:T} P(x)||$. We say there \emph{merely} exists $x$ such that $P(x)$. Additionaly, we have an recursion principle for $||T||$: if $A: \U$ is a mere proposition and we have $f: T \rightarrow A$, then we have a $g: ||T|| \rightarrow A$ suc that $g \circ |\cdot| = f$.

		The identity types of a \emph{set} are propositions: there is only one way to show that two objects are equal. In the same fashion, we can inductively define $n$-types: a $(-2)$-type is a contractible type, and a $(n+1)$-type is a type with identity types that are $n$-types.

		\paragraph{The Univalence Axiom (UA).} A map $A \rightarrow B$ is called an \emph{equivalence} if it is bi-invertible, and we denote $A \simeq B$ the types of equivalences between $A$ and $B$. For types $A,B$, we can define a map $idtoeqv:A=B \rightarrow A \simeq B$: we map $p:A=B$ to $p_*(\text{id}_A)$. The \emph{univalence axiom} states that this map is an equivalence.

	\subsection{Categories in HoTT/UF}

		\paragraph{Precategories.} A precategory $\C$ consists of a type $\C_0$ (or $\C$ for short) of objects, for all objects $a,b: \C$, a \emph{set} $\hom_\C(a,b)$, an associative composition structure and identity morphisms. For $a,b: \C$, we have the type $a=b$ of identities between them and the type $a \cong b$ of isomorphisms between them. By path induction, we can define $idtoiso: a=b \rightarrow a\cong b$, similarly to how we define $idtoeqv$. In fact, if we consider the precategory $Set$ of sets with maps between them, $idtoiso$ is exactly the restriction to sets of $idtoeqv$.

		\paragraph{Univalent categories.} Similarly to the univalence axiom, we want both notions of sameness, identities and isomorphisms, to coincide. We define \emph{univalent} categories (or categories) as precategories such that for all $a,b$, the map $idtoiso$ is an equivalence. In some ways, univalent categories are better behaved than precategories: for example, weak equivalences coincide with isomorphisms, similarly to classical set-theory categories, but they do not for precategories. Not all precategories that arise in practice are univalent: this is one of the reasons why the following results \cite{rezk_completion} are useful and important.

		\begin{theorem}
			\label{thm:tri-cat}

			If $A,B$ are precategories, $C$ is a univalent category and $H: A\rightarrow B$ is a weak equivalence, then $(- \circ H) : C^B \rightarrow C^A$ is an isomorphism of categories.
		\end{theorem}

		\begin{theorem}
			\label{thm:rezk-cat}
			
			For any precategory $A$, there is a univalent category $\wh A$ and a weak equivalence $A \rightarrow \wh A$.
		\end{theorem}

		The second result gives us a way to turn a precategory into a univalent category, while the first result shows that $\wh A$ is the \emph{reflection} of $A$ into categories, meaning functors from $A$ into categories factor essentially uniquely through the weak equivalence $A \rightarrow \wh A$. Theorem \ref{thm:rezk-cat} is known as the Rezk Completion.





\section{Diagram Structures by Example}
	\label{sec:dstr-ex}

	In \cite{up} is designed a language of \emph{signatures} and \emph{structures} which describes in the same way any set-based, categorical or higher-categorical structures. This work was inspired by Makkai's work \cite{folds} on First Order Logic with Dependent Sorts (FOLDS). Here, we will give some examples of diagram structures, and explain our concept of \emph{anonymous objects} for these structures. Its formalization uses the more general notion of \emph{functorial} signatures instead of diagram signatures, and will be detailed in section \ref{sec:fun-sig}. It is expressed within HoTT/UF, or more precisely Two-Level Type Theory (2LTT). Since it is not necessary for our proofs, we will not get into the details of 2LTT.

	\subsection{Structured Sets}

		In a diagram signature, we specify sorts and dependencies between them (hence the name FOLDS). For example, we can define a signature for graphs. The structure of a graph consists of a type of objects $O$, a type of arrows $A$, and for each arrow $a:A$ a source $s(a)$ and a target $t(a)$. We represent this by the following diagram:
		\[
			\begin{tikzcd}
				A \arrow[d, "s" left, shift right] \arrow[d, "t" right, shift left] \\
				O
			\end{tikzcd}
		\]
		Thanks to this \emph{diagram signature}, we can define a graph, which is a \emph{structure} for this signature. For a graph $G$, we have a sort of objects $GO$ and a sort of arrows $GA$, and the corresponding maps $s$ and $t$. When context is clear, we will use $O,A$ instead of $GO,GA$. More conveniently, we can consider a type family $A:\prod_{a,b:O} \U$ instead of the type $A:\U$ with the maps $s,t$. In addition to the signature, we can add axioms that must be verified by its structures: for instance, we would like the types $A(a,b)$ to be mere propositions and $O$ to be a set. A diagram signature together with axioms forms a \emph{diagram theory}.

		Consider two (graph) structures $M,N$. We can define morphisms $\hom(M,N)$ between them: they consist of a map $f: MO \rightarrow NO$, and, for any $a,b: MO$, a map $MA(a,b) \rightarrow NA(fa,fb)$. This gives us the usual notion of graph morphism!

		Similarly, we can define a signature for groups or rings, or even a signature of graph morphisms (for clarity, we omit the names of the arrows on the diagram):
		\[
			\begin{tikzcd}
				A_0 \arrow[d, shift right] \arrow[d, shift left] & F \arrow[dl] \arrow[dr] & A_1 \arrow[d, shift right] \arrow[d, shift left] \\
				O_0 & & O_1
			\end{tikzcd}
		\]
		The pairs $(A_i,O_i)$ are the graphs, and $F$ reprensents the map $O_0 \rightarrow O_1$. We add axioms to ensure $F$ respects the arrows.

	\subsection{Precategories}

		For (pre-)categories, we use the following signature:
		\[
			\begin{tikzcd}
				T \arrow[dr, shift left, bend left, "t_2" description] \arrow[dr, shift right, bend right, "t_0" description] \arrow[dr, "t_1" description] & I \arrow[d,"i"] & E \arrow[dl, shift left, "e_0" right] \arrow[dl, shift right, "e_1" left] \\
				& A \arrow[d, shift left, "c"] \arrow[d, shift right, "d" left] & \\
				& O &
			\end{tikzcd}
		\]
		\[ ci = di, ct_0=dt_1, dt_0=dt_2, ct_1=ct_2, de_1=de_2, ce_1=ce_2 \]
		$O$ represents the objects, $A$ the arrows. An element of $I$ is a witness that the corresponding arrow is the identity, similarly $T$ is the sort of witnesses of composition: for $a:T$, it is a witness that $t_0a \circ t_1a = t_2a$. Lastly, $E$ reprensents equality between arrows. Note that there are composition identities on the arrows between sorts: for example, $ci=di$ guarantees that the identities have the same source and target. In addition to the standard axioms for a category (existence and unicity of a composite for two arrows, identity, associativity) we require that $T,I,E$ be propositions, $E$ an equivalence relation such that $T,I$ respect $E$, and $A$ a set.

		We can then prove that the type of structures for this theory is equivalent to that of pre-categories, and similarly the type of morphisms between them is equivalent to that of category morphisms.

	\subsection{Indiscernibility and Univalent Structures}

		We consider the particular case of (pre-)orders on sets. We use the signature for graphs as above, but add axioms of transitivity and reflexivity. We do not have anti-symmetry yet: in a structure, we could have two objects $a,b: O$ with $a \leq b$ and $b \leq a$ but not $a = b$. However, because of transitivity, $a$ and $b$ would then behave exactly the same in the pre-order: for all $c:O$, we have $(a \leq c) \simeq (b \leq c)$ and $(c \leq a) \simeq (c \leq b)$. This means, $a$ and $b$ are \emph{indiscernible} regarding properties of the pre-order structure. We denote $a \asymp b$ the type of indiscernibilities between $a$ and $b$. See section \ref{sec:fun-sig} for the formalization.

		This is analogous to isomorphic elements of a category: they have the same categorical properties. Thus, we can define a notion of \emph{univalent structure}, analogous to that of univalent category: a structure $M$ is univalent if, for any sort $K$ and any objects $a,b: MK$, the map $idtoind: a=b \rightarrow a \asymp b$ is an equivalence. If we consider the above theory for pre-orders, this gives us anti-symetry: if $a \leq b$ and $b \leq a$, then $a \asymp b$ so $a = b$.

		Additionally, univalence enforces restrictions on the homotopy levels of the sorts. Since $A$ does not depend on any sort, indiscernibilities between objects in $A(a,b)$ for some $a,b:O$ are contractible. Since indiscernibilities and identity types are equivalent, the latter are also contractible. Thus, for a univalent structure, $A(a,b)$ is a mere proposition. Similarly, for $a,b: O$, $a \asymp b$ depends only on $A$, but all $A(x,y)$ are mere propositions (because we consider a univalent structure), thus $a \asymp b$ is a mere proposition. By univalence, $a=b$ is also a mere proposition so $O$ is a set.

		For categories, univalence requires that indiscernible (isomorphic) objects are equal, and guarantees that objects are a $1$-type. We recover the notion of univalent categories!





\section{Anonymous Objects by Example}
	\label{sec:anon-ex}

	During my internship, I developed this concept inspired by the construction of indiscernibilities in \cite{up}. Intuitively, for a Rezk Completion, we quotient objects by indiscernibilities -- only, because indiscernibilities are not mere propositions, we cannot use a set quotient. Thus, our goal is to build a type for which identity types are (equivalent to) the types of indiscernibilities.

	\subsection{Anonymous Objects for pre-orders}

		Once again, consider the example of pre-orders, with the signature given above. We suppose given a structure $M$: it has objects $MO$ and a pre-order relation $MA$ on these objects. Let $T$ be the type of pre-orders over $MO+\mathbb 1$ such that their restriction to $MO$ is $M$.

		There is a natural map $MO \rightarrow T$: for $x: MO$, we duplicate $x$: the duplicate $x'$ compares to every object in the same way as $x$. Denote $\Omega_MO$ the image of this map, and $\omega_MO$ the restriction of this map to $\Omega_MO$. An element of $\Omega_MO$ represents the structure over an object of $M$, without explicitly referring to a specific object in $MO$. This justifies the name \emph{anonymous objects}.

		\begin{example}
			Consider the following pre-order:
			\[
				\begin{tikzcd}
					& B \arrow[dr]\arrow[dd, shift right] & \\
					A \arrow[ur]\arrow[dr]\arrow[rr] & & C \\
					& D \arrow[ur]\arrow[uu, shift right] &
				\end{tikzcd}
			\]
			The anonymous object $\omega_MOB$ looks like this:
			\[
				\begin{tikzcd}
					& B \arrow[dr]\arrow[dd, shift right, bend right]\arrow[d, shift right, red, dashed] & \\
					A \arrow[ur]\arrow[dr]\arrow[r, red, dashed]\arrow[rr, bend left] & |[red]| \square \arrow[u, shift right, red, dashed]\arrow[r, red, dashed]\arrow[d, shift right, red, dashed] & C \\
					& D \arrow[ur]\arrow[uu, shift right, bend right]\arrow[u, shift right, red, dashed] &
				\end{tikzcd}
			\]
			In red, the additional object, which compares to other objects in the same way as $B$.

			Since $B\leq D$ and $D\leq B$, it is the same as $\omega_MOD$: we have $\omega_MOB = \omega_MOD$. In fact, we have $(B \asymp D) \simeq (\omega_MKB = \omega_MKD)$: we will prove this result in general in section \ref{sec:fun-sig}.
		\end{example}

	\subsection{Anonymous Objects for Precategories}

		For pre-categories, the construction is the same as for pre-orders: for a pre-category $\C$, if we consider its sort $O$ of objects, the type of anonymous objects $\Omega_\C O$ is the type of categories $\C'$ with objects $\C_0+1$ such that the restriction of $\C'$ to $\C_0$ is equal to $\C$, and such that the additional object is isomorphic to at least one object in $\C_0$.

		Then, an anonymous object represents a class of isomorphic objects in $\C$: those that are isomorphic (in $\C'$) to the additionnal object. We also have a surjection $\omega_\C O: \C_0 \rightarrow \Omega_\C O$. Intuitively, this is exactly what we want for a \emph{quotient} of $\C_0$ by isomorphisms. In section \ref{sec:rezk-cat}, we use this to give another proof of the Rezk Completion for categories (theorem \ref{thm:rezk-cat}).





\section{Functorial Signatures and Anonymous Objects}
	\label{sec:fun-sig}

	In the previous sections, we gave example of diagram signatures, with sorts and dependence arrows between them. In \cite{up}, the results are proved for a more general and abstract notion of signatures: functorial signatures. In this section, we recall the basics of functorial signatures, and we use this framework to formalize the concept of anonymous objects.

	We define signatures for a given height: for example, structured sets such as graphs have height $2$, and categories have height $3$, as can be seen on their diagrams in section \ref{sec:dstr-ex}: the longest paths on the diagrams have respectively $2$ and $3$ vertices.

	Because \cite{up} treats by induction signatures of any (finite) height, they make use of Two-Level Type Theory \cite{tltt}: in 2LTT, there is a first, meta-theoretical level, with Uniqueness of Identity Proofs (UIP), which is used to define \emph{signatures}, and a second level, similar to HoTT, which is used for \emph{structures}. For a given signature, its structures are defined within the second level, and this makes it possible to transfer any results to plain HoTT. Here, we omit the technical details linked to the use of 2LTT instead of plain HoTT.

	\subsection{Functorial Signatures and Structures}

		\paragraph{Signatures and structures.} We define a category $Sig(n)$ of signatures of height $n$: for example, pre-orders are a signature of height $2$ and categories are a signature of height $3$.

		\begin{definition}[Signature]
			$Sig(0)$ is the trivial category on $\mathbb 1$.

			An object $\LL$ of $Sig(n+1)$ consists of:
			\begin{itemize}
				\item a type $\LL_\bot: \U$.
				\item a functor $(\LL_\bot \rightarrow \U) \rightarrow Sig(n)$, where $\LL_\bot \rightarrow \U$ is the functor category from the discrete category on $\LL_\bot$ to the canonical category $\U$.
			\end{itemize}

			For $\LL, \mathcal M: Sig(n+1)$, a morphism $\alpha: \LL \rightarrow \mathcal M$ consists of:
			\begin{itemize}
				\item a function $\alpha_\bot: \LL_\bot \rightarrow \mathcal M_\bot$
				\item a natural transformation $\alpha'$ as in the diagram:
				\[
					\begin{tikzcd}
					\mathcal M_\bot \rightarrow \U \arrow[dd,"- \circ \alpha_\bot" left] \arrow[rrrd, ""{name=M, below}, "\mathcal M'" above] & & & \\
					& & & Sig(n) \\
					\LL_\bot \rightarrow \U \arrow[rrru, ""{name=L, above}, "\LL'" below] & & & \arrow[from=L, to=M, Rightarrow, "\alpha'" left]
					\end{tikzcd}
				\]
			\end{itemize}
		\end{definition}

		For $\LL: Sig(n+1)$, $\LL_\bot$ is the type of sorts of the bottom level. For a map $M_\bot: \LL_\bot \rightarrow \U$ which gives a type for each sort of the bottom level $\LL_\bot$, we have a signature $\LL'_{M_\bot}: Sig(n)$. It gives the structure over the obejcts in $M_\bot K$, for $K: \LL_\bot$.

		We now define inductively the type $Str(\LL)$ of $\LL$-structures for a given signature $\LL$. A structure for $\LL:Sig(n+1)$ consists of a map $M_\bot : \LL_\bot \rightarrow \U$ and a structure $M'$ for the derived signature $\LL'_{M_\bot}$.

		\begin{definition}[$\LL$-structure]
			If $\LL:Sig(0)$, we define the type of $\LL$-structures $Str(\LL) :\equiv \mathbb 1$.

			For $\LL:Sig(n+1)$, we define:
			\[
				Str(\LL) :\equiv \sum_{M_\bot:\LL_\bot \rightarrow \mathcal U} Str(\LL'_{M_\bot})
			\]
		\end{definition}

		\paragraph{Structure morphisms.} We want $\LL$-structures to form a category. Defining morphisms of $\LL$-structures requires the pullback of an $\mathcal M$-structure along a morphism of signature $\alpha: \LL \rightarrow \mathcal M$.

		\begin{definition}
			[Pullback]

			For $\alpha: \hom_{Sig(n)}(\LL, \mathcal M)$, we define the pullback $\alpha^*: Str(\mathcal M) \rightarrow Str(\LL)$ as follows.

			If $n :\equiv 0$, $\alpha^*$ is the identity.

			If $n > 0$, consider $M:Str(\mathcal M)$. Let $(\alpha^*M)_\bot :\equiv M_\bot \circ \alpha_\bot$. By induction, the morphism $\alpha'_{M_\bot}: \hom_{Sig(n-1}(\LL'_{M_\bot \circ \alpha_\bot}, \mathcal M'_{M_\bot})$ produces
			\[(\alpha'_{M_\bot})^*:Str(\mathcal M'_{M_\bot}) \rightarrow Str(\LL'_{M_\bot \circ \alpha_\bot})\] so we define $(\alpha^*M)' :\equiv (\alpha'_{M_\bot})^*M'$.
		\end{definition}

		We can now define structure morphisms.

		\begin{definition}[Structure morphisms]
			Consider $\LL:Sig(n)$ and $M,N: Str(\LL)$.

			If $n :\equiv 0$, $\hom_{Str(\LL)}(M,N) :\equiv \mathbb 1$.

			If $n > 0$, a morphism $f:\hom(M,N)$ consists of:
			\begin{enumerate}
				\item $f_\bot: \prod_{K: \LL_\bot} M_\bot(K) \rightarrow N_\bot(K)$
				\item $f':\hom_{Str(\LL'_{M_\bot})}(M', (\LL'_{f_\bot})^*N')$
			\end{enumerate}
		\end{definition}

		With similar inductive definition, we can easily define the rest of the category structure on $Str(\LL)$.

	\subsection{Indiscernibility and Univalence}

		The idea behind anonymous objects is inspired by indiscernibilites, for this reason some of the ideas of Section \ref{sec:anon-ex} appear here.

		Consider a $\LL$-structure $M$, $K: \LL_\bot$ and $a,b: M_\bot K$. To define indiscernibilities $a \asymp b$, we add an additional object in $M_\bot K$ that can be replaced by $a$ or $b$: the resulting structures must be equal.

		\begin{definition}[Indicator function]
			Consider $L: \U$, $K:L$, $M: L \rightarrow \U, a: M(K)$. We define the indicator function of $K$ to be:
			\[
				[K]x :\equiv x=K
			\]
			and we define $\wh a: \prod_{x:L}[K](x) \rightarrow M(x)$ by path induction on $\wh a \refl{K} :\equiv a$.
		\end{definition}

		\begin{definition}
			Consider $\LL:Sig(n+1), K: \LL_\bot, M: Str(\LL), a:M_\bot K$. Define:
			\[
				\partial_a M :\equiv (\LL'_{\langle 1_{M_\bot}, \wh a \rangle})^* M' : Str(\LL'_{M_\bot + [K]})
			\]
		\end{definition}
		where $\langle 1_{M_\bot}, \wh a \rangle$ is the induced function on the disjoint union $M_\bot + [K]$.

		Denote $\iota_M$ the canonical injection $\prod_{x: \LL_\bot} M_\bot x \rightarrow (M_\bot+[K])(x)$.

		\begin{definition}[Indiscernibility]
			Consider $\LL:Sig(n+1), K: \LL_\bot, M: Str(\LL), a,b:M_\bot K$. We define the type of indiscernibilities between $a$ and $b$:
			\[
				a \asymp b :\equiv \sum_{p: \partial_a M = \partial_b M} \epsilon_a^{-1} \cdot (\LL'_{\iota_{M_\bot}})^* p \cdot \epsilon_b =_{M'=M'} \refl{M'}
			\]
			where $\epsilon_x$ is the identification
			\[
				(\LL'_{\iota_M})^*\partial_xM' = M'
			\]
		\end{definition}

		We can easily define an identity indiscerniblity $1_x: x \asymp x$, and then a map $idtoind_{x,y}: x=y \rightarrow x \asymp y$ by induction on $idtoind_{x,x}(\refl x) :\equiv 1_x$.

		\begin{definition}
			[Univalence]
			Consider $\LL: Sig(n)$ and $M: Str(\LL)$. If $n :\equiv 0$, $M$ is univalent.

			If $n>0$, for $K: \LL_\bot$, we say $M$ is univalent at $K$ if for all $x,y: M_\bot K$, $idtoind_{x,y}$ is an equivalence. Inductively, we say $M$ is univalent if $M'$ is univalent and if it is univalent at $K$ for all $K$.
		\end{definition}

	\subsection{Anonymous Objects}

		We now formalize our new concept of Anonymous Objects.

		\begin{definition}
			[Anonymous Objects]

			Consider $M: Str(\LL)$. Its \emph{anonymous objects} $\Omega_M: \LL_\bot \rightarrow \mathcal U$ are defined by:
			\begin{align*}
				\Omega_MK :\equiv &\left(N : Str(\LL'_{M_\bot + [K]})\right) \\
				\times &\left( e: (\LL'_{\iota_{M_\bot}})^* N = M' \right) \\
				\times &\left|\left| \sum_{x: M_\bot K} (N,e) = (\partial_x M, \epsilon_x) \right|\right|
			\end{align*}

			We denote $\omega_MK$ the surjection $M_\bot K \rightarrow \Omega_MK$, defined by $\omega_M K :\equiv x \mapsto (\partial_x M, \epsilon_x, |\refl{}|)$.
		\end{definition}

		Indiscernibilities between two objects are the identity types of the corresponding anonymous objects:

		\begin{lemma}
			\label{lem:anon-ind}
			Let $M: Str(\LL), K: \LL_\bot$ and $a,b: M_\bot K$. Then $(\omega_M K a = \omega_M K b) \simeq (a \asymp b)$.
		\end{lemma}

		\begin{proof}
			By definition $a \asymp b :\equiv (p: \partial_a M = \partial_b M) \times (\epsilon_a^{-1} \cdot (\LL'_{\iota_{M_\bot}})^*p \cdot \epsilon_b = \refl{M'})$.

			But $(\epsilon_a^{-1} \cdot (\LL'_{\iota_{M_\bot}})^*p \cdot \epsilon_b = \refl{M'}) \simeq ((\LL'_{\iota_{M_\bot}})^*p^{-1} \cdot \epsilon_a = \epsilon_b) \simeq (p_* \epsilon_a =  \epsilon_b)$, and $(\omega_MKa = \omega_MKb) \simeq (p : \partial_a M = \partial_b M) \times (p_* \epsilon_a = \epsilon_b)$.
		\end{proof}

		In a sense, this shows that anonymous objects generalize the notion of indiscernibilities. Some results of \cite{up} that involve indiscernibilities can be generalized to anonymous objects, and some definitions can be made cleaner: for example, a structure $M$ is univalent at $K: \LL_\bot$ if $\omega_M K$ is an embedding.

		As another example, we have the following truncation result for anonymous objects:

		\begin{lemma}
			For $n>0$, let $\LL: Sig(n)$ and $M: Str(\LL)$. If $M'$ is univalent, then for all $K: \LL_\bot$, $\Omega_MK$ is a $(n-2)$-type.
		\end{lemma}

		\begin{proof}
			We consider fixed $M$ and $K$, with $M'$ univalent. In \cite{up}, the proof of Theorems 16.10 and 16.11 also proves that, if $M'$ is univalent, then for all $a,b:M_\bot K$, $a \asymp b$ is a $(n-3)$-type.

			Since $\omega_MK$ is a surjection, Lemma \ref{lem:anon-ind} proves that identity types in $\Omega_MK$ are $(n-3)$-types.
		\end{proof}





\section{Another Rezk Completion Construction for Categories}
	\label{sec:rezk-cat}

	In this section, we give a new proof of Theorem \ref{thm:rezk-cat}, making use of anonymous objects. This proof is partly inspired by the proof of Theorem \ref{thm:tri-cat} that is given in \cite{hott}.

	We consider a fixed category $\C$. For $x,y:\C$, we have an equivalence $indtoiso: x \asymp y \xrightarrow{\sim} x \cong y$.

	Because there is no other bottom-level sort, we will abusively use $\Omega$ and $\omega$ instead of $\Omega_\C O$ and $\omega_\C O$. We want to define morphisms between two objects $x,y: \Omega$ that are compatible with the (surjective) map $\omega$, but avoiding the use of the axiom of choice to choose some $a,b: \C$ such that $\omega a=x,\omega b=y$. For this, we take a set of morphisms that is compatible with \emph{all} pre-images of $x,y$. We define:
	\begin{align*}
		T_{x,y} :\equiv~&(S : Set) \\
				\times &\left(k: \prod_{a,b:\C} \prod_{\substack{p:\omega a = x\\ q:\omega b = y}} S = \C(a,b) \right) \\
				\times &\left(\prod_{a,b,a',b':\C} \prod_{\substack{p:\omega a = x\\ q:\omega b = y\\p':\omega a'=x\\q': \omega b'=y}}
						k_{a,b}(p,q)^{-1} \cdot k_{a',b'}(p',q') = indtoiso(q \cdot q'^{-1}) \circ (-) \circ indtoiso(p' \cdot p^{-1}) \right)
	\end{align*}
	By function extensionality, we omit conversions between equivalences and identities. Here, $\circ$ is the composition of morphisms in $\C$.

	\begin{lemma}
		\label{lem:Txy-contr}
		$T_{x,y}$ is contractible.
	\end{lemma}

	\begin{proof}
		Because our goal is a mere proposition, we can assume $a^0, b^0: \C$ with $p^0: \omega a^0 = x$ and $q^0: \omega b^0 = y$. Let $S^0 :\equiv \C(a^0,b^0)$. Then, for $a,b,p,q$ as above, we have $k^0_{a,b}(p,q) :\equiv indtoiso((q_0) \cdot q^{-1}) \circ (-) \circ indtoiso(p \cdot (p^0)^{-1}) : S^0 = \C(a,b)$. Associativity of composition gives us the last term of the dependant sum. Thus, $T_{x,y}$ is inhabited.

		Let $(S, k, e) : T_{x,y}$. Then we have $k_{a^0,b^0}(p^0,q^0): S = S^0$. Next we need to prove that $(k_{a^0,b^0}(p^0,q^0))_* k = k^0$. But that is exactly $e$ applied to $a^0,b^0,p^0,q^0$. Lastly, the third term of the dependent sum $T_{x,y}$ is a mere proposition, thus we have $(S, k, -) = (S^0, k^0, -)$.
	\end{proof}

	Similarly, we define composition, identity and equality on morphisms in $\Omega$. For brevity, we only detail the proof for composition.

	The idea is the same: we want composition in $\Omega$ to match composition in $\C$, without depending on a particular pre-image of our objects in $\Omega$. For $x_1,x_2,x_3: \Omega$, $f: \Omega(x_1,x_2)$, $g: \Omega(x_2,x_3)$ and $h:\Omega(x_1,x_3)$, we define:
	\begin{align*}
		&H_{x_1,x_2,x_3}(f,g,h):\equiv (P: Prop) \\
		&\times \left( \prod_{a_1, a_2, a_3: \C} \prod_{p_i: \omega a_i = x_i} P = \C_{a_1,a_2,a_3}((k_{a_1,a_2}(p_1, p_2))_*f, (k_{a_2,a_3}(p_2,p_3))_* g, (k_{a_1,a_3}(p_1,p_3))_*h ) \right)
	\end{align*}
	where $\C_{a_1,a_2,a_3}(u,v,w)$ denotes the type of witnesses that $v \circ u = w$, and $k$ is obtained using the contractibility of $T_{x_i,x_j}$.

	\begin{lemma}
		$H_{x_1,x_2,x_3}(f,g,h)$ is contractible.
	\end{lemma}

	\begin{proof}
		Because our goal is a mere proposition, we can assume $a_1^0, a_2^0, a_3^0$ and $p_1^0, p_2^0, p_3^0$ as above. Then, we define:
		\[P^0 :\equiv \C_{a_1^0, a_2^0, a_3^0}\left(k_{a_1^0,a_2^0}(p_1^0, p_2^0))_*f, (k_{a_2^0,a_3^0}(p_2^0,p_3^0))^* g, (k_{a_1^0,a_3^0}(p_1^0,p_3^0))_*h\right)\]
		For any $a_1,a_2,a_3,p_1,p_2,p_3$ as above, we have $p_i^0 \cdot p_i^{-1} : a_i^0 \asymp a_i$. In addition to the third term of $T_{x_i,x_j}$ which allows us to go from $k_{a_i^0,a_j^0}$ to $k_{a_i,a_j}$, these indiscernibilities allow us to go from $\C_{a_i^0}$ to $\C_{a_i}$, which gives us the second term. Thus, $H_{x_1,x_2,x_3}(f,g,h)$ is inhabited.

		Consider now another inhabitant $(P, z)$. Then, $z_{a_1^0,a_2^0,a_3^0}(p_1^0,p_2^0,p_3^0)$ gives us exactly $P = P^0$. Since the second term is a mere proposition, this concludes the proof.
	\end{proof}

	We have, by construction, a weak equivalence $\omega: \hom(\C, \Omega_\C)$. This implies that $\Omega_\C$ respects the axioms of precategories. It remains to prove that $\Omega_\C$ is univalent, ie $\omega_{\Omega_\C}$ is an embedding. For this, we use the following lemma:

	\begin{lemma}
		\label{lem:transp-omega-ind}
		Consider $x,y: \Omega$, $a,b: \C$ with $p : x=y$, $q: \omega a = x$ and $r: \omega b = y$.

		We have natural maps:
		\begin{itemize}
			\item $indtoiso(q \cdot p \cdot r^{-1}) \circ (-) :\C(a,a) \xrightarrow{\sim} \C(a,b)$
			\item $transp_p : \Omega(x,x) \xrightarrow{\sim} \Omega(x,y)$
			\item $k_{a,a}(q,q): \Omega(x,x) \xrightarrow{\sim} \C(a,a)$
			\item $k_{a,b}(q,r): \Omega(x,y) \xrightarrow{\sim} \C(a,b)$
		\end{itemize}

		These maps commute:

		\[
			\begin{tikzcd}
				\C(a,a) \arrow[r, "a \asymp b"] & \C(a,b) \\
				\Omega(x,x) \arrow[u, "k_{a,a}({q,q})"] \arrow[r, "transp_p" below]
					& \Omega(x,y) \arrow[u, "k_{a,b}({q,r})" right]
			\end{tikzcd}
		\]
	\end{lemma}

	\begin{proof}
		By induction, we may assume $x \equiv y$ and $p \equiv \refl{x}$. Then, the bottom map is the identity. Moreover, the third term of $T_{x,y}$ gives us:
		\[
			k_{a,b}(q,r)^{-1} \cdot k_{a,a}(q,q) = \text{indtoiso}(r \cdot q^{-1}) \circ (-) \circ \text{indtoiso}(q \cdot q^{-1})
		\]
		Since $indtoiso(q \cdot q^{-1}) = 1$ (and $indtoiso$ preserves composition), this gives us exactly the inverse of the map $indtoiso(q \cdot p \cdot r^{-1}) \circ (-)$, which concludes the proof.
	\end{proof}

	\begin{lemma}
		\label{lem:univ-rezk-cat}
		$\Omega_\C$ is a univalent category.
	\end{lemma}

	\begin{proof}
		Consider $x,y: \Omega$. We want to prove that the application of $\omega(x,y)$ to the identity type of $x,y:\C_0$, which we denote $\text{ap}_\omega(x,y): (x=y) \rightarrow (\omega x = \omega y)$, is an equivalence. Since this is a mere proposition, we may assume we have $a,b: \C$ with $q: \omega a = x$ and $r: \omega b = y$.

		We have the following maps:
		\[
			\begin{tikzcd}
				a \cong b \arrow[r, "\omega"] & x \cong y \\
				\omega a = \omega b \arrow[u, "indtoiso" left] & x = y \arrow[u, "idtoiso" right] \arrow[l, "q \cdot (-) \cdot r^{-1}" below]
			\end{tikzcd}
		\]
		Since $\omega$ is a weak equivalence, all but the right map are equivalences. Thus, proving that this square commutes proves that $idtoiso$ and thus $idtoind = indtoiso^{-1} \circ idtoiso$ are equivalences.

		Let's look into these maps more precisely:
		\begin{itemize}
			\item $\text{idtoiso}(p) :\equiv transp_p(1_x) = transp_p(k_{a,a}(q,q)^{-1}(1_a))$
			\item $\omega(e) :\equiv k_{a,b}(q,r)^{-1} e$
			\item $\text{indtoiso}(q \cdot p \cdot r^{-1}) = \text{indtoiso}(q \cdot p \cdot r^{-1}) \circ 1_a$
		\end{itemize}
		We recognize the maps of Lemma \ref{lem:transp-omega-ind}: the commutativity of the above square is the Lemma applied on $1_a$.

		This concludes our proof that $idtoind_{x,y}$ is an equivalence, for all $x,y$, thus $\Omega$ is univalent.
	\end{proof}

	We have built a univalent category $\Omega$ with a weak equivalence $\C \rightarrow \Omega$, which concludes our proof of Theorem \ref{thm:rezk-cat}.





\section*{Conclusion}
	\label{sec:ccl}
	
	Building on the concept of indiscernibilities, we defined the notion of anonymous objects, then used it to give a new proof of the Rezk Completion for categories.

	The notion of anonymous objects proved to be slightly more general than that of indiscernibilities, and hopefully can give us a better understanding of functorial signatures and structures as a whole. In particular, it seems particularly well-suited for a generalization of the Rezk Completion, as it works as a quotient of objects by indiscernibilities. Unfortunately, it is not always possible to build a well-behaved Rezk Completion (see appendix \ref{sec:counter-ex}). We would need some form of associativity of indiscernibilites, similarly to the associative isomorphisms of categories: in some way, the structure must "pass to the quotient". Although I defined this for a very restricted case (signatures of height $n :\equiv 3$, and working only with some cases of diagram structures), some work in progress by David Wärn about higher equivalence relations in homotopy type theory might help formalize these intuitions in a more general way.




\newpage
\printbibliography
\newpage

\appendix

\section{A Counter-Example of Theorem \ref{thm:tri-cat} for General Signatures}
	\label{sec:counter-ex}

	TODO

\end{document}
