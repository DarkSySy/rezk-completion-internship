\documentclass{article}


% Packages
	\usepackage[utf8]{inputenc}
	\usepackage{color}
	\usepackage{verbatim}
	\usepackage{listings}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{amsthm}
	\usepackage{mathrsfs}
	\usepackage{graphicx}
	\usepackage{bbm}
	\usepackage{biblatex}
	\usepackage[unicode]{hyperref}
	\usepackage{indentfirst}
	% \usepackage[standard]{ntheorem}
	\usepackage{algorithm}
	\usepackage[noend]{algpseudocode}
	\usepackage{tikz}
	\usepackage{tikz-cd}
	\usepackage{enumitem}

% New commands and environments
	\newcommand{\hs}{\hspace{0.7cm}}
	\newcommand{\be}{\begin{equation}}
	\newcommand{\ee}{\end{equation}}
	\newcommand{\bee}{\begin{eqnarray}}
	\newcommand{\eee}{\end{eqnarray}}
	\newcommand{\fin}{\nonumber\\}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\LL}{\mathcal{L}}
	\newcommand{\U}{\mathcal{U}}
	\newcommand{\C}{\mathcal{C}}
	\newcommand{\wh}[1]{\widehat{#1}}
	\newcommand{\refl}[1]{\text{refl}_{#1}}
	\newcommand{\fo}[2]{\text{Fanout}_{#1}(#2)}
	\newcommand{\itoi}{\text{idtoind}}
	\newcommand{\itoc}{\text{idtocon}}
	\newcommand{\itoe}{\text{idtoeqv}}
	\newcommand{\id}[1]{\textbf{id}_{#1}}
	\newtheorem{theorem}{Theorem}[section]
	\newtheorem{lemma}[theorem]{Lemma}
	\newtheorem{definition}[theorem]{Definition}
	\newtheorem{example}[theorem]{Example}
	% \newtheorem{corollary}{Corollary}
	% \newtheorem{prop}{Proposition}

\title{About Rezk Completion for Higher Categorical Structures}
\author{Sylvain Gay}

\addbibresource{biblio.bib}

\begin{document}

\maketitle

\section{Preliminaries}
	
	We will use the notations of \cite{up}, in particular chapters 16 and 17. We will freely use the notations $(a:A) \times (B a)$ and $\sum_{a:A} B a$ for dependent sum types.

	For a signature $\LL$, there may be an axiom $\mathcal T$ on $Str(\LL)$, by abuse of notation we denote $Str(\LL)$ the type of structures satisfying $\mathcal T$. For $M_\bot: \LL_\bot \rightarrow \U$, $\LL'_{M_\bot}$ is equipped with the axiom $M' \mapsto \mathcal(M_\bot, M')$, and the same abuse of notation holds.


	Unless otherwise specified, we consider a fixed signature $\LL : Sig(n)$, with $n>0$. % The restriction $n>0$ is a stylistic choice: we avoid to specify the (trivial) case $n=0$ for most of our definitions and results.

	We define \emph{weak equivalences} by analogy with categories. Although they are not named in \cite{up}, they play an important role, in particular it is exactly the hypothesis for Lemma 17.5 which is central in \cite{up}.

	\begin{definition}[Weak Equivalence]
		For $M, N: Str(\LL)$, a morphism $f: M \rightarrow N$ is a \emph{weak equivalence} if $f_\bot$ is a (fiberwise) surjection and $f'$ is an equivalence.
	\end{definition}

	% \subsection{Useful general lemmas}

	% 	\begin{lemma}
	% 		Let $A,B,C : \U$, $g: A\rightarrow B$ and $f: B \rightarrow C$. If $g$ is a surjection and $f \circ g$ is an equivalence, then $g$ is an equivalence.
	% 	\end{lemma}

	% 	\begin{proof}
	% 		We prove that the fibers of $g$ are contractible. $g$ is a surjection, thus they are (merely) inhabited. Let $y: B$ and $x_1,x_2 : A$ with $p_i: gx_i = y$. Then $\text{ap}_{(f \circ g)^{-1} \circ f}(p_1 \cdot p_2^{-1}): x_1 = x_2$
	% 	\end{proof}




\section{Anonymity}

	\begin{definition}[Anonymous objects]
		Consider $M: Str(\LL)$. Its \emph{anonymous objects} $\Omega_M: \LL_\bot \rightarrow \mathcal U$ are defined by:
		\begin{align*}
			\Omega_MK :\equiv &\left(N : Str(\LL'_{M_\bot + [K]})\right) \\
			\times &\left( e: (\LL'_{\iota_{M_\bot}})^* N = M' \right) \\
			\times &\left|\left| \sum_{x: M_\bot K} (N,e) = (\partial_x M, \epsilon_x) \right|\right|
		\end{align*}

		We denote $\omega_M$ the (fiberwise) surjection $M_\bot \rightarrow \Omega_M$, defined by $\omega_M K :\equiv x \mapsto (\partial_x M, \epsilon_x, |\refl{}|)$.
	\end{definition}

	\begin{example}[Anonymous objects for categories]
		For (pre-)categories, there is only one $K: \LL_\bot$, it is the sort $O$ of objects. Then, $Str(\LL'_{M_\bot + [O]})$ is the type of categories with objects $\C + 1$.

		For such a category $N$, the condition $(\LL'_{\iota_{M_\bot}})^* N = M'$ guarantees that the full subcategory of $N$ with only the objects of $\C$ is indeed $\C$.

		Lastly, the condition $\left|\left| \sum_{x: M_\bot K} (N,e) = (\partial_x M, \epsilon_x) \right|\right|$ guarantees that there merely exists an object $x: \C$ such that $N$ is the same as $\C$ with a duplicate of $x$. In other words, the additional object is isomorphic to $x$.

		This is the \emph{anonymous} part: the category $N$ represents the categorical structure over the additional object without choosing an object of $\C$ as a reference, $x$ only merely exists.
	\end{example}

	An anonymous object represents the structure over the object without consideration for the object itself: in essence, this is what we need for a Rezk completion. Indiscernibilities between objects are exactly the paths between their corresponding anonymous objects:

	\begin{lemma}
		Let $M: Str(\LL), K: \LL_\bot$ and $a,b: M_\bot K$. Then $(\omega_M K a = \omega_M K b) \simeq a \asymp b$.
	\end{lemma}

	\begin{proof}
		By definition $a \asymp b :\equiv (p: \partial_a M = \partial_b M) \times (\epsilon_a^{-1} \cdot (\LL'_{\iota_{M_\bot}})^*p \cdot \epsilon_b = \refl{M'})$.

		But $(\epsilon_a^{-1} \cdot (\LL'_{\iota_{M_\bot}})^*p \cdot \epsilon_b = \refl{M'}) \simeq ((\LL'_{\iota_{M_\bot}})^*p^{-1} \cdot \epsilon_a = \epsilon_b) \simeq (p_* \epsilon_a =  \epsilon_b)$, and $(\omega_MKa = \omega_MKb) \simeq (p : \partial_a M = \partial_b M) \times (p_* \epsilon_a = \epsilon_b)$.
	\end{proof}

	Because of this, we can translate some definitions and results that revolve around indiscernibilities to the context of anonymous objects. For example, proving univalence at $K$ is exactly proving that $\omega_M K$ is an embedding. The following lemma is another example.

	\begin{lemma}
		\label{lem:WE-Omega}

		Let $M, N: Str(\LL)$ and $f: M \rightarrow N$ a weak equivalence. Then there is a map $f_\omega^{-1}: \Omega_N \rightarrow \Omega_M$ such that the following square commutes:

		\[
			\begin{tikzcd}
				\Omega_M & \Omega_N \arrow[l, "f_\omega^{-1}" above] \\
				M_\bot \arrow[u, "\omega_M" left] \arrow[r, "f_\bot" below] & N_\bot \arrow[u, "\omega_N" right] \\
			\end{tikzcd}
		\]
	\end{lemma}

	This result is very close to Lemma 17.5 of \cite{up} and only slightly more general: the result in \cite{up} gives this square for the corresponding path types instead of the types themselves. Because of this, the proof is also very similar, and we will use some elements of it.

	\begin{proof}
		We fix a weak equivalence $f$. $f'$ is an equivalence, so we have a path $M' = (\LL'_{f_\bot})^* N'$. By path induction, we may assume $M' \equiv (\LL'_{f_\bot})^* N'$. We fix $K: \LL_\bot$.

		Consider $(P, e, -): \Omega_N K$. Let $\tilde P :\equiv (\LL'_{f_\bot + 1})^* P : Str(\LL'_{M_\bot + [K]})$.

		We have $(\LL'_{f_\bot})^* e : (\LL'_{f_\bot})^*(\LL'_{\iota_N})^* P = (\LL'_{f_\bot})^* N' \equiv M'$. The proof of Lemma 17.5 in \cite{up} gives us $\alpha: (\LL'_{\iota_M})^* \circ (\LL'_{f_\bot})^* \equiv (\LL'_{f_\bot})^* \circ (\LL'_{\iota_N})^*$. Thus, we have a path $\tilde e :\equiv \text{ap}_\alpha(P) \cdot (\LL'_{f_\bot})^* e : (\LL'_{\iota_M})^* \tilde P = M'$.

		It remains to show that there (merely) exists an object $\tilde x: M_\bot K$ such that $(\tilde P, \tilde e) = \omega_M(\tilde x)$. Since this is a mere proposition, we may assume we have some $x: N_\bot K$ such that $(P, e) = \omega_N(x)$. Since $f_\bot K$ is a surjection, and again our goal is a proposition, we may assume we have some $\tilde x: M_\bot K$ such that $f_\bot K(\tilde x) = x$. By path induction, we may assume both these paths are $\refl{}$.

		Then, \cite{up} gives us a path $\beta_xN: \tilde P \equiv (\LL'_{f_\bot+1})^*\partial_{f_\bot K(\tilde x)}N = \partial_{\tilde x} M$. We now need a path $(\beta_xN)_* \tilde e = \epsilon_{\tilde x}$. But this is exactly the (two-dimensional) identification $\sigma$ in \cite{up}. Thus $(\tilde P, \tilde e) = \omega_M(\tilde x)$.

		We have defined $f_\omega^{-1} K: \Omega_N K \rightarrow \Omega_M K$. Moreover, the above proof also shows that $f_\omega^{-1}K \circ \omega_NK \circ f_\bot K(\tilde x) = \omega_MK(\tilde x)$, which gives us (by function extensionnality) the required commutative square.
	\end{proof}

	Because $f_\omega^{-1} \circ \omega_N \circ f_\bot = \omega_M$ and $\omega_M$ is a surjection, $f_\omega^{-1}$ is also a surjection.




\section{Another Rezk Construction for Categories}

	We give another construction of the Rezk Completion for categories, with the hope that it can be more easily and systematically generalized to other structures.

	Consider a category $\C$. Its type of anonymous objects $\Omega_\C$ is a $1$-type. We want to define morphisms between two objects $x,y:\Omega$ that are compatible with the map $\omega$ but which do not depend on a particular pre-images of $x,y$, that is on some $a,b:\C$ such that $\omega a = x$ and $\omega b = y$. For this, we take a set of morphisms that is by definition compatible with \emph{all} pre-images of $x,y$. We define:
	\begin{align*}
		T_{x,y} :\equiv~&(S : Set) \\
				\times &\left(k: \prod_{a,b:\C} \prod_{\substack{p:\omega a = x\\ q:\omega b = y}} S = \C(a,b) \right) \\
				\times &\left(\prod_{a,b,a',b':\C} \prod_{\substack{p:\omega a = x\\ q:\omega b = y\\p':\omega a'=x\\q': \omega b'=y}}
						k_{a',b'}(p',q') \circ k_{a,b}(p,q)^{-1} = \text{indtoiso}(q \cdot q'^{-1}) \circ (-) \circ \text{indtoiso}(p' \cdot p^{-1}) \right)
	\end{align*}
	(By function extensionnality, we omit conversions between equivalences and identities.)

	This construction is strongly inspired by the proof of Theorem 9.9.4 in \cite{hott}. We have a similar result:

	\begin{lemma}
		\label{lem:Txy-contr}
		$T_{x,y}$ is contractible.
	\end{lemma}

	\begin{proof}
		Because our goal is a mere proposition, we can assume $a^0, b^0: \C$ with $p^0: \omega a^0 = x$ and $q^0: \omega b^0 = y$. Let $S^0 :\equiv \C(a^0,b^0)$. Then, for $a,b,p,q$ as above, we have $k^0_{a,b}(p,q) :\equiv \text{indtoiso}((q_0) \cdot q^{-1}) \circ (-) \circ \text{indtoiso}(p \cdot (p^0)^{-1}) : S^0 = \C(a,b)$. Associativity of composition gives us the last term of the dependant sum. Thus, $T_{x,y}$ is inhabited.

		Let $(S, k, e) : T_{x,y}$. Then we have $k_{a^0,b^0}(p^0,q^0): S = S^0$. Next we need to prove that $(k_{a^0,b^0}(p^0,q^0))_* k = k^0$. But that is exactly $e$ applied to $a^0,b^0,p^0,q^0$. Lastly, the third term of the dependent sum $T_{x,y}$ is a mere proposition, thus we have $(S, k, -) = (S^0, k^0, -)$.
	\end{proof}

	$\prod_{x,y} T_{x,y}$ is also contractible, and we can define $\Omega(x,y)$ to be the projection from this into $Set$.

	Similarly, we define composition, identity and equality on morphisms in $\Omega$. For brevity, we only detail the proof for composition, the others are identical.

	The idea is the same: we want our composition in $\Omega$ to match composition in $\C$, but not depend on a particular pre-image of our objects in $\Omega$. For $x_1,x_2,x_3 : \Omega$, $f: \Omega(x_1,x_2)$, $g: \Omega(x_2,x_3)$ and $h: \Omega(x_1, x_3)$, we define:

	\begin{align*}
		&H_{x_1,x_2,x_3}(f,g,h):\equiv (P: Prop) \\
		&\times \left( \prod_{a_1, a_2, a_3: \C} \prod_{p_i: \omega a_i = x_i} P = \C_{a_1,a_2,a_3}((k_{a_1,a_2}(p_1, p_2))_*f, (k_{a_2,a_3}(p_2,p_3))_* g, (k_{a_1,a_3}(p_1,p_3))_*h ) \right)
	\end{align*}
	where $\C_{a_1,a_2,a_3}(u,v,w)$ denotes the type of witnesses that $v \circ u = w$, and $k$ is obtained using the contractibility of $T_{x_i,x_j}$.

	\begin{lemma}
		$H_{x_1,x_2,x_3}(f,g,h)$ is contractible.
	\end{lemma}

	\begin{proof}
		Because our goal is a mere proposition, we can assume $a_1^0, a_2^0, a_3^0$ and $p_1^0, p_2^0, p_3^0$ as above. Then, we define:
		\[P^0 :\equiv \C_{a_1^0, a_2^0, a_3^0}\left(k_{a_1^0,a_2^0}(p_1^0, p_2^0))_*f, (k_{a_2^0,a_3^0}(p_2^0,p_3^0))^* g, (k_{a_1^0,a_3^0}(p_1^0,p_3^0))_*h\right)\]
		For any $a_1,a_2,a_3,p_1,p_2,p_3$ as above, we have $p_i^0 \cdot p_i^{-1} : a_i^0 \asymp a_i$. In addition to the third term of $T_{x_i,x_j}$ which allows us to go from $k_{a_i^0,a_j^0}$ to $k_{a_i,a_j}$, these indiscernibilities allow us to go from $\C_{a_i^0}$ to $\C_{a_i}$, which gives us the second term. Thus, $H_{x_1,x_2,x_3}(f,g,h)$ is inhabited.

		Consider now another inhabitant $(P, z)$. Then, $z_{a_1^0,a_2^0,a_3^0}(p_1^0,p_2^0,p_3^0)$ gives us exactly $P = P^0$. Since the second term is a mere proposition, this concludes the proof.
	\end{proof}

	We have, by construction, a weak equivalence $\omega: \hom(\C, \Omega_\C)$. It remains to prove that $\Omega_\C$ is univalent. For this, we use the following lemma:

	\begin{lemma}
		\label{lem:transp-omega-ind}
		Consider $x,y: \Omega$, $a,b: \C$ with $p : x=y$, $q: \omega a = x$ and $r: \omega b = y$.

		We have natural maps:
		\begin{itemize}
			\item $\text{indtoiso}(q \cdot p \cdot r^{-1}) \circ (-) :\C(a,a) \xrightarrow{\sim} \C(a,b)$
			\item $transp_p : \Omega(x,x) \xrightarrow{\sim} \Omega(x,y)$
			\item $k_{a,a}(q,q): \Omega(x,x) \xrightarrow{\sim} \C(a,a)$
			\item $k_{a,b}(q,r): \Omega(x,y) \xrightarrow{\sim} \C(a,b)$
		\end{itemize}

		These maps commute:

		\[
			\begin{tikzcd}
				\C(a,a) \arrow[r, "a \asymp b"] & \C(a,b) \\
				\Omega(x,x) \arrow[u, "k_{a,a}({q,q})"] \arrow[r, "transp_p" below]
					& \Omega(x,y) \arrow[u, "k_{a,b}({q,r})" right]
			\end{tikzcd}
		\]
	\end{lemma}

	\begin{proof}
		By induction, we may assume $x \equiv y$ and $p \equiv \refl{x}$. Then, the bottom map is the identity. Moreover, the third term of $T_{x,y}$ gives us:
		\[
			k_{a,a}(q,q) \circ k_{a,b}(q,r)^{-1} = \text{indtoiso}(r \cdot q^{-1}) \circ (-) \circ \text{indtoiso}(q \cdot q^{-1})
		\]
		Since $\text{indtoiso}(q \cdot q^{-1}) = 1$, this gives us exactly the inverse of the map $\text{indtoiso}(q \cdot p \cdot r^{-1}) \circ (-)$, which concludes the proof.
	\end{proof}

	\begin{theorem}
		\label{thm:univ-rezk-cat}
		$\Omega_\C$ is a univalent category.
	\end{theorem}

	\begin{proof}
		Consider $x,y: \Omega$. We want to prove that $\text{ap}_\omega(x,y)$ is an equivalence. Since this is a mere proposition, we may assume we have $a,b: \C$ with $q: \omega a = x$ and $r: \omega b = y$.

		We have the following maps:
		\[
			\begin{tikzcd}
				a \cong b \arrow[r, "\omega"] & x \cong y \\
				\omega a = \omega b \arrow[u, "\text{indtoiso}" left] & x = y \arrow[u, "\text{idtoiso}" right] \arrow[l, "q \cdot (-) \cdot r^{-1}" below]
			\end{tikzcd}
		\]
		Since $\omega$ is a weak equivalence, all but the right map are equivalences. Thus, proving that this square commutes proves that $idtoiso$ and thus $idtoind$ are equivalences.

		Let's look into these maps more precisely:
		\begin{itemize}
			\item $\text{idtoiso}(p) :\equiv transp_p(1_x) = transp_p(k_{a,a}(q,q)^{-1}(1_a))$
			\item $\omega(e) :\equiv k_{a,b}(q,r)^{-1} e$
			\item $\text{indtoiso}(q \cdot p \cdot r^{-1}) = \text{indtoiso}(q \cdot p \cdot r^{-1}) \circ 1_a$
		\end{itemize}
		We recognize the maps of Lemma \ref{lem:transp-omega-ind}: the commutativity of the above square is the Lemma applied on $1_a$.

		This concludes our proof that $idtoind_{x,y}$ is an equivalence, for all $x,y$, thus $\Omega$ is univalent.
	\end{proof}




\section{Generalization}

	\subsection{Associativity of indiscernibilities}

		The first challenge to generalize the above construction is to understand what $indtoiso$ means in a more general context. In Lemma \ref{lem:Txy-contr}, we use the following properties of isomorphisms (denoting $a_0 \cong a_1$ the type of isomorphisms):

		\begin{itemize}
			\item left composition $a_0 \cong a_1 \rightarrow \C(b, a_0) \rightarrow \C(b, a_1)$
			\item right composition $\C(a_1, b) \rightarrow a_0 \cong a_1 \rightarrow \C(b, a_0)$
			\item associativity between these compositions: $(e \circ (-)) \circ f = e \circ ((-) \circ f)$, where $e,f$ are isomorphisms and $\circ$ denotes left and right composition as above.
		\end{itemize}

		For any (diagram) signature, we have the first two items: it is simply the action of the indiscernibilities on the dependent sorts. However, the third is not always true.

		\begin{example}[Multigraphs]
			We consider the following signature for multigrphs:
			\[
				\begin{tikzcd}
					E \arrow[d, shift left] \arrow[d, shift right] \\
					A \arrow[d, shift left] \arrow[d, shift right] \\
					O
				\end{tikzcd}
			\]
			$O$ represents objects, $A$ arrows and $E$ equality between arrows. We take axioms such that arrows are sets and $E$ is the identity type for these sets. Then, this signature doesn't have associativity of indiscernibilities. Consider the following graph:
			\[
				\begin{tikzcd}
					A_1 & A_2 \\
					B_1 \arrow[u, shift left] \arrow[u, shift right] \arrow[ur, shift left] \arrow[ur, shift right] &
					B_2 \arrow[u, shift left] \arrow[u, shift right] \arrow[ul, shift left] \arrow[ul, shift right]
				\end{tikzcd}
			\]
			Denote $G$ this graph, and $G(X,Y)$ the set of arrows between $X,Y$. Clearly, $A_1 \asymp A_2$ and $B_1 \asymp B_2$ are inhabited. Indiscernibilities between $B_1$ and $B_2$ are determined by a pair of identities $(G(B_1,A_1) = G(B_2,A_1)) \times (G(B_1,A_2) = G(B_2,A_2))$, similarly indiscernibilities between $A_1$ and $A_2$ are determined by a pair $(G(B_1,A_1) = G(B_1,A_2)) \times (G(B_2,A_1) = G(B_2,A_2))$.

			Associativity of indiscernibilities would mean that, for any such pairs $e: A_1 \asymp A_2$ and $f: B_1 \asymp B_2$, the following diagram commutes:
			\[
				\begin{tikzcd}
					G(B_1,A_2) \arrow[r, "\pi_2(f)", equal] & G(B_2,A_2) \arrow[d, "\pi_2(e)", equal] \\
					G(B_1,A_1) \arrow[u, "\pi_1(e)", equal] & G(B_2,A_1) \arrow[l, "\pi_1(f)", equal]
				\end{tikzcd}
			\]
			But all of these types are equivalent to $\mathbbm 2$, so this is clearly false: we can take three of these identities to be the identity and the third to be the non trivial involution in $\mathbbm 2$.
		\end{example}

		In most cases, we study structures with some form of \emph{isomorphisms}, such as in categories: associativity for indiscernibilities is then a simple consequence of associativity for morphisms.

		Before formally defining associativity for indiscernibilities, we define some notations. For now, we only consider diagram signatures with finite fanouts, for $n :\equiv 3$. For clarity, all sorts named $K_i$ are sorts of the bottom level, all sorts named $A_i$ are sorts of the middle level and all sorts named $P_i$ are sorts of the top level. We write $A\{K_1, \dots, K_m\}$ if $A$ depends on $K_1, \dots, K_m$. This means, for a structure $M$ and a family $k: \prod MK_i$, we have a sort $A(k_1, \dots, k_m): \LL'_{M_\bot}$. Moreover, for an indiscernibility $e: k_j \asymp k_j'$, we denote $A(k_1, \dots, k_{j-1}, e, k_{j+1}, \dots, k_m)$ the identity it yields between $A(\dots, k_j, \dots)$ and $A(\dots, k_j', \dots$.

		We now formally define signatures with associative indiscernibilities, for $n:\equiv 3$.

		\begin{definition}
			[Diagram signature with associative indiscernibilities, $n:\equiv 3$]

			Let $\LL$ be a diagram signature with finites fanouts, for $n:\equiv 3$. We say it has \emph{associative indiscernibilities} if, for all middle level sort $A{K_1, \dots, K_m}$, for all $1 \leq j < l \leq m$, for all $k: \prod_{i=1,\dots,n} K_i$, $k_j': K_j$, $k_l': K_l$ and indiscernibilities $e_j : k_j \asymp k_j'$ and $e_l: k_l \asymp k_l'$, the following diagram commutes:
			\[
				\begin{tikzcd}
					A(\dots, k_j', \dots, k_l, \dots) \arrow[r, "A{(\dots, k_j', \dots, e_l, \dots)}", equal] &
					A(\dots, k_j', \dots, k_l', \dots) \arrow[d, "A{(\dots, e_j, \dots, k_l', \dots)}", equal] \\
					A(\dots, k_j, \dots, k_l, \dots) \arrow[u, "A{(\dots, e_j, \dots, k_l, \dots)}", equal] &
					A(\dots, k_j, \dots, k_l', \dots) \arrow[l, "A{(\dots, k_j, \dots, e_l, \dots)}", equal]
				\end{tikzcd}
			\]
		\end{definition}

		Remark that no mention is made of top-level sorts. Indeed, for the Rezk Completion, we will (by induction) work with structures $M$ such that $M'$ is univalent, thus top-level sorts are mere propositions and a result analog to the above is trivial.

	\subsection{Signature with isomorphisms}

		For categories, indiscernibilities are embedded into the signature as isomorphisms: this property is important in the proof of Theorem \ref{thm:univ-rezk-cat}. We define a signature with isomorphisms in the following way:

		\begin{definition}
			[Signature with isomorphisms, $n:\equiv 3$]

			Consider a signature $\LL : Sig(3)$. We say it is a signature with isomorphisms if, for any sort $K$ of the bottom level, we have a sort $Iso\{K,K\}$ of isomorphisms, a sort $Refl$ that depends on $Iso$ and a sort $Comp$ for composition in $Iso$. For $k_1,k_2: MK$, we can define $indtoiso_{k_1,k_2}(e):\equiv Iso(k_1,e)_* Refl(k_1)$. We require this map to be an equivalence.
		\end{definition}

		This doesn't directly work for categories for example, but we can add an isomorphisms sort in the signature categories, and make the necessary adjustments to make it the correct subset of the morphisms.

		This is a work in progress: ultimately, we should not be needing such a strong hypothesis, but for now it makes the proof simpler.

	\subsection{Univalent replacement}

		We fix a diagram signature $\LL: Sig(3)$ with finite fanouts, associative indiscernibilities and isomorphisms. Consider a structure $M: Str(\LL)$ such that $M'$ is univalent. We build a univalent structure $\wh M: Str(\LL)$ with a weak equivalence $M \rightarrow \wh M$.

		\paragraph{Bottom level.} We define $\wh M_\bot :\equiv \Omega_M$. We have an obvious (fiberwise) surjection $\omega_M: M_\bot \rightarrow \wh M_\bot$.

		\paragraph{Middle level.} Consider $A\{K_1, \dots, K_m\}$. For $k: \prod_i \wh M_\bot K_i$, we define:
		\begin{align*}
			T_k &:\equiv (S: Set) \\
			&\times \left(\kappa:\prod_{x: \prod_i MK_i} \prod_{p: \prod_i \omega_M x_i = k_i} S = MA(x) \right) \\
			&\times \left(\mu: \prod_{x,y: \prod_i MK_i} \prod_{\substack{p: \prod_i \omega_M x_i = k_i \\ q: \prod_i \omega_M y_i = k_i}} \kappa(x,p)^{-1} \cdot \kappa(y, q) = A(p \cdot q^{-1}) \right)
		\end{align*}
		where $A(p\cdot q^{-1})$ denotes the application, in any order, of the indiscernibilities $p_i \cdot q_i^{-1}$: the order does not matter because of associativity. We have the same result as in Lemma \ref{lem:Txy-contr}:
		\begin{lemma}
			\label{lem:Tk-contr}
			$T_k$ is contractible.
		\end{lemma}
		We can now define $\wh M A(k) :\equiv \pi_1(t)$, where $t$ is the unique inhabitant of $T_k$.

		\paragraph{Top level.} Consider $P_{\{K_1, \dots, K_m\}}\{A_1, \dots, A_l\}$. For clarity, we do not specify on which $K_i$s each $A_j$ depends. For $k: \prod_i \wh M K_i$ and $a: \prod_i \wh M A_i$, we define:
		\begin{align*}
			U_{k,a} &:\equiv (P: Prop) \\
			&\times \left( \prod_{x: \prod_i MK_i} \prod_{p: \prod_i \omega_M x_i = k_i} P = MP_x(\kappa(x,p)_* a) \right)
		\end{align*}
		Here, $\kappa(x,p)_* a$ is an abuse of notation: for all $a_i$, we pull it back to the structure $M$ using the identities $\kappa$ defined above. However, it is not $\kappa(x,p)$ exactly since $A_i$ need not depend on all $K_i$s, and can depend on them multiple times. Again, we have a contractibility result:
		\begin{lemma}
			\label{lem:Uka-contr}
			$U_{k,a}$ is contractible.
		\end{lemma}
		We define $\wh M P_k(a) :\equiv \pi_1(u)$, where $u$ is the unique inhabitant of $U_{k,a}$.

		\paragraph{Weak equivalence.} By construction, we have $M' = (\LL'_{\omega_M})^* \wh M'$, and thus a weak equivalence $M \rightarrow \wh M$.

		\paragraph{Univalence, top level.} By construction, the top level consists only of mere propositions, thus it is univalent.

		\paragraph{Univalence, middle level.} Let $a_1, a_2: \wh MA(k_1, \dots, k_m)$. We want to prove that $\itoi_{a_1,a_2}: a_1 = a_2 \rightarrow a_1 \asymp a_2$ is an equivalence. Since this is a mere proposition, we can assume $x_1, \dots, x_m$ with $p_i:\omega_M x_i = k_i$. Let $\alpha_{1,2} :\equiv \kappa(x,p)_* a_{1,2}$. Then, we have a map $a_1 \asymp a_2 \rightarrow \alpha_1 \asymp \alpha_2$. Consider the following diagram. Since all these types are mere propositions and $\itoi_{\alpha_1,\alpha_2}$ is an equivalence by univalence of $M'$, this concludes our proof that $\itoi_{a_1,a_2}$ is an equivalence.
		\[
			\begin{tikzcd}
				\alpha_1 \asymp \alpha_2 & a_1 \asymp a_2 \arrow[l] \\
				\alpha_1 = \alpha_2 \arrow[u, "\itoi"] \arrow[u, "\sim" marking, phantom, shift right] \arrow[r, "\kappa{(x,p)}" below] & a_1 = a_2 \arrow[u, "\itoi" right]
			\end{tikzcd}
		\]

		\paragraph{Univalence, bottom level.} Consider $K: \LL_\bot$ and $x_1,x_2: \wh M_\bot K$. We want to prove that $\itoi_{x_1,x_2}$ is an equivalence. Since our goal is a mere proposition, we may assume $a_1,a_2: M_\bot K$ with $p_i:\omega_M a_i = x_i$. Then, we have the following maps:
		\[
			\begin{tikzcd}
				Iso(a_1,a_2) & Iso(x_1,x_2)
				\arrow[l, "\kappa{(a,p)}_*" above]
				\arrow[l, phantom, "\sim" marking, shift left] \\
				& x_1 \asymp x_2
				\arrow[u, "indtoiso_{x_1,x_2}" right]
				\arrow[u, phantom, "\sim" marking, shift left] \\
				\omega a_1 = \omega a_2
				\arrow[uu, "indtoiso_{a_1,a_2}"]
				\arrow[uu, phantom, "\sim" marking, shift right] &
				x_1 = x_2 \arrow[u, "\itoi_{x_1,x_2}"right]
				\arrow[l, "p_1 \cdot {(-)} \cdot p_2^{-1}" below]
				\arrow[l, phantom, "\sim" marking, shift right] \\
			\end{tikzcd}
		\]
		We now prove this diagram commutes. More precisely, we prove that, for all $x_1,x_2,a_1,a_2,p_1,p_2$ and $q: x_1=x_2$, the image by both compositions is the same. By induction, we may assume $x_1 \equiv x_2$ and $q$ is $\refl{x_1}$. By definition, $indtoiso \circ \itoi(\refl{x_1}) :\equiv Refl(x_1)$ (where $Refl(x_1)$ denotes reflexivity for $Iso$), moreover $indtoiso_{a_1,a_2}(p_1 \cdot p_2^{-1}) :\equiv Iso(a_1,p_1 \cdot p_2^{-1})_* Refl(a_1)$. Thus, we only need to prove:
		\[
			\kappa(a,p)_* Refl(x_1) = Iso(a_1,p_1 \cdot p_2^{-1})_* Refl(a_1)
		\]
		The third therm of $T_k$, $\mu$, gives us:
		\[
			\kappa((a_1;a_1),(p_1;p_1))^{-1} \cdot \kappa(a, p) = Iso(p_1 \cdot p_1^{-1}, p_1 \cdot p_2^{-1})
		\]
		Because $\kappa$ preserves $Refl$, we have $\kappa((a_1;a_1),(p_1;p_1))^{-1}_* Refl(x_1) = Refl(a_1)$ which concludes our proof.

	\subsection{Rezk Completion}

		We remain under the same assumptions as in the previous section.

		For the univalent completion to behave as we expect, we need an analog to Theorem 9.9.4 of \cite{hott}, which states:

		\begin{theorem}
			If $A,B$ are precategories, $C$ is a category, and $H : A \rightarrow B$ is a weak equivalence, then $( - \circ H ) : C^B \rightarrow C^A$ is an isomorphism.
		\end{theorem}

		For general signatures, we do not have a category structure on the morphism types $\hom(B,C)$ and $\hom(A,C)$, thus we can only expect the following (weaker) result:

		% \begin{theorem}
		% 	\label{thm:triangle}

			If $A,B,C$ are structures such that $C$ is univalent, aand $H : A \rightarrow B$ is a weak equivalence, then $( - \circ H): \hom(B,C) \rightarrow \hom(A,C)$ is an equivalence.
		% \end{theorem}

		For now, we only have a proof of surjectivity, adapted from the prof of Theorem 9.9.4 of \cite{hott}
		\begin{theorem}
			\label{thm:tri-surj}

			If $A,B,C$ are structures such that $C$ is univalent, and $H : A \rightarrow B$ is a weak equivalence, then $( - \circ H): \hom(B,C) \rightarrow \hom(A,C)$ is an surjective.
		\end{theorem}

		We use the following lemma:

		\begin{lemma}
			\label{lem:lift-anon-univ}
			If $A,C$ are structures, $C$ is univalent and $F: \hom(A,C)$, then there is $F_\omega: \Omega_A \rightarrow C_\bot$ such that $F_\omega \circ \omega_A = F_\bot$.
		\end{lemma}

		\begin{proof}
			[Proof of Theorem \ref{thm:tri-surj}]

			Consider $F: \hom(A,C)$. We build $G: \hom(B,C)$ such that $G \circ H = F$. We define $G_\bot :\equiv F_\omega \circ H^{-1}_\omega \circ \omega_B$ (where $F_\omega$ is defined in Lemma \ref{lem:lift-anon-univ}, $H^{-1}_\omega$ in Lemma \ref{lem:WE-Omega}). Then:
			\begin{align*}
				G_\bot \circ H_\bot &= (F_\omega \circ H^{-1}_\omega \circ \omega_B) \circ H_\bot \\
				&= F_\omega \circ (H^{-1}_\omega \circ \omega_B \circ H_\bot) \\
				&= F_\omega \circ \omega_A \\
				&= F_\bot
			\end{align*}

			We now need to build $G'$. Let $\alpha\{K_1,\dots K_m\}$ a middle level sort, and $b:\prod_i BK_i$. Then, for $e: B\alpha(b)$ we define:
			\begin{align*}
				W_{b,e} &:\equiv (\varepsilon : C\alpha(G_\bot b)) \\
				&\times \left(\nu: \prod_{a: \prod_i AK_i} \prod_{p:\prod_i Ha_i = b_i} F_{\alpha(a)} \circ H^{-1}_{\alpha(a)}(e) = \varepsilon \right)
			\end{align*}

			We show $W_{b,e}$ is contractible. Our goal is a proposition, so we may assume $a^0: \prod_i aK_i$ with $p^0: \prod_i Ha_i = b_i$. Then, we define $\epsilon^0 :\equiv F_{\alpha(a^0)} \circ H_{\alpha(a^0)}^{-1}$.

			For the top level sorts, since $C$ is univalent, these are mere propositions, which makes the result trivial.
		\end{proof}




\section{Proofs}

	\begin{proof}[Lemma \ref{lem:Tk-contr}]
		TODO
	\end{proof}

	\begin{proof}[Lemma \ref{lem:Uka-contr}]
		TODO
	\end{proof}

	\begin{proof}[Lemma \ref{lem:lift-anon-univ}]
		Consider $K: \LL_\bot$. For $x: \Omega_AK$, we define:
		\begin{align*}
			V_x &:\equiv (y: C_\bot K) \\
			& \times \left(\kappa: \prod_{a: A_\bot K}\prod_{p:\omega_Aa = x} f_\bot a = y\right) \\
			& \times \left(\prod_{a_{0,1}: A_\bot K} \prod_{p_i: \omega_A a_i = x} F_{Iso} \circ indtoiso(p_0 \cdot p_1^{-1}) = idtoiso\left(\kappa(a_0,p_0) \cdot \kappa(a_1,p_1)^{-1}\right) \right)
		\end{align*}
		$V_x$ is contractible. Since this is a mere proposition, we may assume $a^0: A_\bot K$ and $p^0: \omega_A a^0 = x$. We define $y^0 :\equiv F_\bot a^0$. Then, we have $\kappa^0(a,p) :\equiv idtoiso^{-1} \circ F_{Iso} \circ indtoiso(p \cdot (p^0)^{-1})$. For the last term, we use the fact that $F$ preserves $Comp$. This proves that $V_x$ is inhabited.
		Consider now another $(y, \kappa, \mu): V_x$. $\kappa(a^0,p^0)$ gives us $f_\bot a_0 = y$, and $\mu(a', a^0, p', p^0)$ gives us $\kappa(a',p') = \kappa^0(a',p') \cdot \kappa(a^0,p^0)^{-1}$, thus by function extensionality $\kappa(a^0,p^0)_* \kappa^0 = \kappa$. Since the last term is a mere proposition, this concludes the proof that $V_x$ is contractible. We may now define $F_\omega x :\equiv y$.
	\end{proof}

% \subsection{Signatures with isomorphisms}

%  	For category-like structures, the map $f_\omega^{-1}$ define in Lemma \ref{lem:WE-Omega} behaves particularly well. Identities in $\Omega_M, \Omega_N$ are equivalent to isomorphisms in $M, N$, but because we have a weak equivalence $f: M \rightarrow N$, isomorphisms in $M$ are equivalent to isomorphisms in $N$. A consequence is that $f_\omega^{-1}$ is an equivalence (we already know it is a surjection. This motivates the following definition.

% 	\begin{definition}[Signature with isomorphisms]
% 		We say that $\LL$ \emph{has isomorphisms at} $K: \LL_\bot$ when for any $M,N: Str(\LL)$ and any weak equivalence $f: M \rightarrow N$, $f_\omega^{-1} K$ is an embedding.

% 		We define signatures with isomorphisms by induction.

% 		\begin{itemize}
% 			\item When $n:\equiv 0$, every $\LL: Str(0)$ is a signature with isomorphisms.
% 			\item When $n>0$, $\LL: Sig(n)$ is a signature with isomorphisms if:
% 				\begin{enumerate}[label=(\roman*)]
% 					\item for any $K: \LL_\bot$, $\LL$ has isomorphisms at $K$.
% 					\item for any $M_\bot: \LL_\bot \rightarrow \U$, $\LL'_{M_\bot}$ is a signature with isomorphisms.
% 				\end{enumerate}
% 		\end{itemize}
% 	\end{definition}


% % 	TODO: Explain methods to prove that a signature is a signature with isomorphisms:
% % 	\begin{itemize}
% % 		\item always true when $n :\equiv 1,2$
% % 		\item using connectedness. This works with an assumption very similar to having a \emph{categorical} $\LL$. In particular, it works for categories and I expect it to be the easiest proof for category-like structures.
% % 	\end{itemize}


% \subsection{Respecting anonymity}

% 	We need some form of Lemma \ref{lem:transp-omega-ind} for a general signature $\LL$. Remark that, for isomrphisms, $k^{-1}_{a,b}$ corresponds to $\text{ap}_{(\omega_M)_\omega^{-1}}(a,b)$. This explains the following definition:

% 	\begin{definition}
% 		We define what it means for a signature $\LL$ to \emph{respect anonymity}.
% 		\begin{itemize}
% 			\item when $n:\equiv 0$, all signatures $\LL$ respect anonymity.
% 			\item when $n>0$, $\LL$ respects anonymity if:
% 			\begin{itemize}
% 				\item for all $M: Str(\LL)$ such that $M'$ is univalent, there is a univalent $N: Str(\LL'_{\Omega_M})$ such that $M' = (\LL'_{\omega_M})^*N$.
% 				\item for all $M$ as above, $K: \LL$, $a_0,a_1,b_0,b_1: M_\bot K$, $p:\omega_M a_0 = \omega_M a_1$ and $q:\omega_M b_0 = \omega_M b_1$ we have:
% 				\[
% 					p^{-1} \cdot (-) \cdot q = \text{ap}_{(\omega_M)^{-1}_\omega \circ \omega_{\Omega}}(p^{-1} \cdot (-) \cdot q)
% 				\]

% 				\item for all $M_\bot : \LL_\bot \rightarrow \U$, $\LL'_{M_\bot}$ respects anonymity.
% 			\end{itemize}
% 		\end{itemize}
% 	\end{definition}

% \section{Towards Rezk Completion}

% 	As opposed to categories, for a general signature the morphisms $\hom(M,N)$ between two structures do not have an associated category structure. Therefore, we can only define Rezk Completion for them in this weaker way:

% 	\begin{definition}
% 		A \emph{Rezk Completion} of a structure $M$ is a univalent structure $\wh M$ with a morphism $H: \hom(M, \wh M)$ such that, for all univalent $N$, $(- \circ H) : \hom(\wh M, N) \rightarrow \hom(M, N)$ is an equivalence.
% 	\end{definition}

% 	For now, we want to build, for a structure $M$, a univalent $\wh M$ with a surjective weak equivalence $H : M \rightarrow \wh M$. We probably need more hypotheses on $\LL$ to prove that this gives us a Rezk Completion.

% 	We want structure over the anonymous objects $\Omega_M$, to make it into a $\LL$-structure. More precisely, we want to be able to transpose the structure over $M_\bot$ to $\Omega_M$: in general, this is not possible and would require some form of the Axiom of Choice -- we would need a section $s$ of $\omega_M$ in order to define the structure over $\Omega_M$ as the structure over $s(\Omega_M)$ given by $M'$. Thus, we need the following on $\LL$:

% 	\begin{definition}
% 		We define what it means for a signature $\LL$ to \emph{respect anonymity}.
% 		\begin{itemize}
% 			\item when $n:\equiv 0$, all signatures $\LL$ respect anonymity.
% 			\item when $n>0$, $\LL$ respects anonymity if:
% 			\begin{itemize}
% 				\item for all $M: Str(\LL)$ such that $M'$ is univalent, there is a univalent $N: Str(\LL'_{\Omega_M})$ such that $M' = (\LL'_{\omega_M})^*N$.
% 				\item for all $M_\bot : \LL_\bot \rightarrow \U$, $\LL'_{M_\bot}$ respects anonymity.
% 			\end{itemize}
% 		\end{itemize}
% 	\end{definition}

% 	We will later give general methods to prove that a signature $\LL$ respects anonymity, in particular for categorical structures. For now, we prove our general result:

% 	\begin{theorem}
% 		If $\LL$ respects anonymity, then for all $M: Str(\LL)$ there is a univalent $\wh M: Str(\LL)$ with a surjective weak equivalence $H : \hom(M, \wh M)$.
% 	\end{theorem}

% 	% \begin{proof}
% 	% 	By induction on $\LL$. If $n :\equiv 0$, all structures are univalent so this is trivial. Consider $n > 0$. By induction, we may assume $M'$ is univalent. Then, there is a univalent $N: Str(\LL'_{\Omega_M})$ such that $M' = (\LL'_{\omega_M})^*N$. We define $\wh M :\equiv (\Omega_M, N)$. We have a weak equivalence $H: M \rightarrow \wh M$. Since $\wh M' :\equiv N$ is univalent, we only need to prove that, for all $K: \LL_\bot$, $\omega_{\wh M}K$ is an embedding. We fix such a $K$ and omit it for the rest of the proof.

% 	% 	Lemma \ref{lem:WE-Omega} gives us $H_\omega^{-1}: \Omega_{\wh M}K \rightarrow \Omega_MK$ such that $\omega_M = H_\omega^{-1} \circ \omega_{\wh M} \circ H_\bot \equiv H_\omega^{-1} \circ \omega_{\wh M} \circ \omega_M$. Because $\omega_M$ is a surjection, we have:
% 	% 	\begin{align*}
% 	% 		&\prod_{x: \Omega_M} \left|\left|H_\omega^{-1} \circ \omega_{\wh M}(x) = x\right|\right| \\
% 	% 		\rightarrow &\prod_{x,y: \Omega_M} \left|\left| (H_\omega^{-1} \circ \omega_{\wh M}(x) = x)
% 	% 												 \times (H_\omega^{-1} \circ \omega_{\wh M}(y) = y) \right|\right| \\
% 	% 		\rightarrow &\prod_{x,y: \Omega_M} isEquiv(\text{ap}_{H_\omega^{-1} \circ \omega_{\wh M}}(x,y))
% 	% 	\end{align*}

% 	% 	We proved $H_\omega^{-1} \circ \omega_{\wh M}$ is an embedding, but it is also a surjection since $\omega_M$ is a surjection and $\omega_M = H_\omega^{-1} \circ \omega_{\wh M} \circ H_\bot$. Thus, it is an equivalence.
% 	% \end{proof}




\printbibliography

\end{document}
